package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectUpdateByIdRequest(
            @Nullable String token
    ) {
        super(token);
    }

    public ProjectUpdateByIdRequest(
            @Nullable String token,
            @Nullable String projectId,
            @Nullable String name,
            @Nullable String description
    ) {
        super(token);
        this.projectId = projectId;
        this.name = name;
        this.description = description;
    }

}
