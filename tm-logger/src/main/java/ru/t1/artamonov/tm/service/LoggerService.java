package ru.t1.artamonov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.artamonov.tm.api.IPropertyService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public final class LoggerService {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String mongoHost = propertyService.getHostName();

    @NotNull
    private final Integer mongoPort = propertyService.getPort();

    @NotNull
    private final MongoClient mongoClient = new MongoClient(mongoHost, mongoPort);

    @NotNull
    private final String mongoDbName = propertyService.getDbName();

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase(mongoDbName);

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @SneakyThrows
    public void log(@NotNull final String json) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(json, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null) mongoDatabase.createCollection(collectionName);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}
